package ru.t1.amsmirnov.taskmanager.dto.request.data;

import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.request.AbstractUserRequest;

public final class DataJsonLoadJaxbRequest extends AbstractUserRequest {

    public DataJsonLoadJaxbRequest() {
    }

    public DataJsonLoadJaxbRequest(@Nullable String token) {
        super(token);
    }

}
