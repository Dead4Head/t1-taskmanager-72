package ru.t1.amsmirnov.taskmanager.dto.response.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.response.AbstractResultResponse;

public final class DataBase64LoadResponse extends AbstractResultResponse {

    public DataBase64LoadResponse() {
    }

    public DataBase64LoadResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }


}
