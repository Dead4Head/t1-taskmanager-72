package ru.t1.amsmirnov.taskmanager.dto.request.task;

import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.request.AbstractUserRequest;
import ru.t1.amsmirnov.taskmanager.enumerated.TaskSort;

public final class TaskListRequest extends AbstractUserRequest {

    @Nullable
    private TaskSort sort;

    public TaskListRequest() {
    }

    public TaskListRequest(
            @Nullable final String token,
            @Nullable final TaskSort sort
    ) {
        super(token);
        setSort(sort);
    }

    @Nullable
    public TaskSort getSort() {
        return sort;
    }

    public void setSort(@Nullable final TaskSort sort) {
        this.sort = sort;
    }

}