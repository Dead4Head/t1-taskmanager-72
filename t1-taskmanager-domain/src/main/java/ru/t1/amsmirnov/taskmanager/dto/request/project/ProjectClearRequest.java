package ru.t1.amsmirnov.taskmanager.dto.request.project;

import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.request.AbstractUserRequest;

public final class ProjectClearRequest extends AbstractUserRequest {

    public ProjectClearRequest() {
    }

    public ProjectClearRequest(@Nullable String token) {
        super(token);
    }

}
