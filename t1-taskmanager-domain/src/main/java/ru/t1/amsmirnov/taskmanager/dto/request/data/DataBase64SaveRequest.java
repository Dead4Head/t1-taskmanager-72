package ru.t1.amsmirnov.taskmanager.dto.request.data;

import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.request.AbstractUserRequest;

public final class DataBase64SaveRequest extends AbstractUserRequest {

    public DataBase64SaveRequest() {
    }

    public DataBase64SaveRequest(@Nullable final String token) {
        super(token);
    }

}
