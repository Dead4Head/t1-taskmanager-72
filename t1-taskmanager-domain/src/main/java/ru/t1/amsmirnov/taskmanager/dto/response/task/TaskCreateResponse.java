package ru.t1.amsmirnov.taskmanager.dto.response.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.model.TaskDTO;

public final class TaskCreateResponse extends AbstractTaskResponse {

    public TaskCreateResponse() {
    }

    public TaskCreateResponse(@Nullable final TaskDTO task) {
        super(task);
    }

    public TaskCreateResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}