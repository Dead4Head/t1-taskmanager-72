package ru.t1.amsmirnov.taskmanager.dto.response.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.response.AbstractResultResponse;

public final class DataBackupSaveResponse extends AbstractResultResponse {

    public DataBackupSaveResponse() {
    }

    public DataBackupSaveResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}
