package ru.t1.amsmirnov.taskmanager.dto.request.user;

import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.request.AbstractUserRequest;

public final class UserChangePasswordRequest extends AbstractUserRequest {

    @Nullable
    private String password;

    public UserChangePasswordRequest() {
    }

    public UserChangePasswordRequest(
            @Nullable final String token,
            @Nullable final String password
    ) {
        super(token);
        setPassword(password);
    }

    @Nullable
    public String getPassword() {
        return password;
    }

    public void setPassword(@Nullable final String password) {
        this.password = password;
    }

}