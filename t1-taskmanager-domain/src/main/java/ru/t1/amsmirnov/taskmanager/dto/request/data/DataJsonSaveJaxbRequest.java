package ru.t1.amsmirnov.taskmanager.dto.request.data;

import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.request.AbstractUserRequest;

public final class DataJsonSaveJaxbRequest extends AbstractUserRequest {

    public DataJsonSaveJaxbRequest() {
    }

    public DataJsonSaveJaxbRequest(@Nullable String token) {
        super(token);
    }

}
