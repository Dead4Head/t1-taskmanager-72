package ru.t1.amsmirnov.taskmanager.dto.response.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.response.AbstractResultResponse;

public final class TaskBindToProjectResponse extends AbstractResultResponse {

    public TaskBindToProjectResponse() {
    }

    public TaskBindToProjectResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}