package ru.t1.amsmirnov.taskmanager.unit.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.t1.amsmirnov.taskmanager.config.ApplicationConfiguration;
import ru.t1.amsmirnov.taskmanager.config.DatabaseConfiguration;
import ru.t1.amsmirnov.taskmanager.dto.ProjectWebDto;
import ru.t1.amsmirnov.taskmanager.exception.entity.ModelNotFoundException;
import ru.t1.amsmirnov.taskmanager.marker.DBCategory;
import ru.t1.amsmirnov.taskmanager.service.dto.ProjectDtoService;
import ru.t1.amsmirnov.taskmanager.util.UserUtil;

import java.util.ArrayList;
import java.util.List;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DatabaseConfiguration.class, ApplicationConfiguration.class})
@Category(DBCategory.class)
public class ProjectServiceTest {

    @NotNull
    @Autowired
    private ProjectDtoService projectService;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    private final ProjectWebDto project1 = new ProjectWebDto("Test Project 1");

    @NotNull
    private final ProjectWebDto project2 = new ProjectWebDto("Test Project 2");

    @Before
    public void initTest() throws Exception {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("test", "test");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        project1.setUserId(UserUtil.getUserId());
        project2.setUserId(UserUtil.getUserId());
        projectService.add(project1);
        projectService.add(project2);
    }

    @After
    public void cleanTest() throws Exception {
        projectService.removeAll();
    }

    @Test
    public void findAllByUserIdTest() throws Exception {
        Assert.assertEquals(2, projectService.findAll(UserUtil.getUserId()).size());
    }

    @Test
    public void findOneByIdAndUserIdTest() throws Exception {
        Assert.assertNotNull(projectService.findOneById(UserUtil.getUserId(), project1.getId()));
        Assert.assertNotNull(projectService.findOneById(UserUtil.getUserId(), project2.getId()));
    }

    @Test(expected = ModelNotFoundException.class)
    public void removeByIdAndUserIdTest() throws Exception {
        @NotNull final List<ProjectWebDto> projects = new ArrayList<>();
        projects.add(project1);
        Assert.assertNotNull(projectService.findOneById(UserUtil.getUserId(), project1.getId()));
        projectService.removeOneById(UserUtil.getUserId(), project1.getId());
        //Expected ModelNotFoundException cause deleted
        projectService.findOneById(UserUtil.getUserId(), project1.getId());
    }

    @Test
    public void removeByUserIdTest() throws Exception {
        projectService.removeOne(UserUtil.getUserId(), project1);
        projectService.removeOne(UserUtil.getUserId(), project2);
        Assert.assertEquals(0, projectService.findAll(UserUtil.getUserId()).size());
    }

}
