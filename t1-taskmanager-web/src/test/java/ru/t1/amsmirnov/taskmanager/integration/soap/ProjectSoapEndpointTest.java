package ru.t1.amsmirnov.taskmanager.integration.soap;

import org.apache.cxf.helpers.CastUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.http.HttpHeaders;
import ru.t1.amsmirnov.taskmanager.api.IAuthEndpoint;
import ru.t1.amsmirnov.taskmanager.api.IProjectEndpoint;
import ru.t1.amsmirnov.taskmanager.client.AuthSoapClient;
import ru.t1.amsmirnov.taskmanager.client.ProjectSoapClient;
import ru.t1.amsmirnov.taskmanager.dto.ProjectWebDto;
import ru.t1.amsmirnov.taskmanager.marker.IntegrationCategory;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.soap.SOAPFaultException;
import javax.xml.ws.handler.MessageContext;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Category(IntegrationCategory.class)
public class ProjectSoapEndpointTest {

    @NotNull
    private static final String TEST_USER = "test";

    @NotNull
    private static final String TEST_PASS = "test";

    @NotNull
    private static final String URL = "http://localhost:8080";

    @NotNull
    private static IAuthEndpoint authEndpoint;

    @NotNull
    private static IProjectEndpoint projectEndpoint;

    @Nullable
    private static ProjectWebDto project;

    @NotNull
    final String test_text = "TEST_SOAP_ENDPOINT_CREATE";

    @BeforeClass
    public static void beforeClass() throws Exception {
        authEndpoint = AuthSoapClient.getInstance(URL);
        Assert.assertTrue(authEndpoint.login(TEST_USER, TEST_PASS).getSuccess());
        projectEndpoint = ProjectSoapClient.getInstance(URL);
        @NotNull final BindingProvider authBindingProvider = (BindingProvider) authEndpoint;
        @NotNull final BindingProvider projectBindingProvider = (BindingProvider) projectEndpoint;
        Map<String, List<String >> headers = CastUtils.cast((Map)authBindingProvider.getResponseContext().get(MessageContext.HTTP_RESPONSE_HEADERS));
        if(headers == null) headers = new HashMap<>();
        @NotNull final Object cookieValue = headers.get(HttpHeaders.SET_COOKIE);
        @NotNull final List<String> cookies = (List<String>) cookieValue;
        headers.put(HttpHeaders.COOKIE, Collections.singletonList(cookies.get(0)));
        projectBindingProvider.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS, headers);
    }

    @Before
    public void before() {
        project = projectEndpoint.create(test_text, test_text);
    }

    @After
    public void after() {
        projectEndpoint.deleteAll();
    }

    @Test
    public void testCreate() {
        final String text = test_text + "_CREATE";
        project = projectEndpoint.create(text, text);
        final ProjectWebDto foundProject = projectEndpoint.findById(project.getId());
        Assert.assertEquals(project.getId(), foundProject.getId());
    }

    @Test
    public void testFindById() {
        final ProjectWebDto foundedProject = projectEndpoint.findById(project.getId());
        Assert.assertEquals(project.getName(), foundedProject.getName());
        Assert.assertEquals(project.getId(), foundedProject.getId());
    }

    @Test(expected = SOAPFaultException.class)
    public void testDelete() {
        projectEndpoint.delete(project.getId());
        projectEndpoint.findById(project.getId());
    }

}
