package ru.t1.amsmirnov.taskmanager.unit.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.amsmirnov.taskmanager.config.ApplicationConfiguration;
import ru.t1.amsmirnov.taskmanager.dto.TaskWebDto;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.marker.DBCategory;
import ru.t1.amsmirnov.taskmanager.repository.dto.TaskDtoRepository;
import ru.t1.amsmirnov.taskmanager.util.UserUtil;

@Transactional
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class})
@Category(DBCategory.class)
public class TaskRepositoryTest {

    @NotNull
    private final TaskWebDto task1 = new TaskWebDto("Test Task 1");

    @NotNull
    private final TaskWebDto task2 = new TaskWebDto("Test Task 2");

    @NotNull
    @Autowired
    private TaskDtoRepository taskRepository;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @Before
    public void initTest() throws AbstractException {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("test", "test");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        task1.setUserId(UserUtil.getUserId());
        task2.setUserId(UserUtil.getUserId());
        taskRepository.save(task1);
        taskRepository.save(task2);
    }

    @After
    public void clean() throws AbstractException {
        taskRepository.deleteByUserId(UserUtil.getUserId());
    }

    @Test
    public void findAllByUserIdTest() throws AbstractException {
        Assert.assertNotNull(taskRepository.findAllByUserId(UserUtil.getUserId()));
        Assert.assertEquals(2, taskRepository.findAllByUserId(UserUtil.getUserId()).size());
    }

    @Test
    public void findByUserIdAndIdTest() throws AbstractException {
        Assert.assertNotNull(taskRepository.findByUserIdAndId(UserUtil.getUserId(), task1.getId()));
        Assert.assertNotNull(taskRepository.findByUserIdAndId(UserUtil.getUserId(), task2.getId()));
    }

    @Test
    public void deleteAllByUserIdTest() throws AbstractException {
        taskRepository.deleteByUserId(UserUtil.getUserId());
        Assert.assertEquals(0, taskRepository.findAllByUserId(UserUtil.getUserId()).size());
    }

    @Test
    public void deleteByUserIdAndIdTest() throws AbstractException {
        taskRepository.deleteByUserIdAndId(UserUtil.getUserId(), task1.getId());
        Assert.assertNull(taskRepository.findByUserIdAndId(UserUtil.getUserId(), task1.getId()));
    }

}
