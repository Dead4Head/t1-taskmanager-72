package ru.t1.amsmirnov.taskmanager.unit.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.t1.amsmirnov.taskmanager.config.ApplicationConfiguration;
import ru.t1.amsmirnov.taskmanager.config.DatabaseConfiguration;
import ru.t1.amsmirnov.taskmanager.dto.TaskWebDto;
import ru.t1.amsmirnov.taskmanager.exception.entity.ModelNotFoundException;
import ru.t1.amsmirnov.taskmanager.marker.DBCategory;
import ru.t1.amsmirnov.taskmanager.service.dto.TaskDtoService;
import ru.t1.amsmirnov.taskmanager.util.UserUtil;

import java.util.ArrayList;
import java.util.List;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class, DatabaseConfiguration.class})
@Category(DBCategory.class)
public class TaskServiceTest {

    @NotNull
    @Autowired
    private TaskDtoService taskService;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    private final TaskWebDto task1 = new TaskWebDto("Test Task 1");

    @NotNull
    private final TaskWebDto task2 = new TaskWebDto("Test Task 2");

    @Before
    public void initTest() throws Exception {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("test", "test");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        task1.setUserId(UserUtil.getUserId());
        task2.setUserId(UserUtil.getUserId());
        taskService.add(task1);
        taskService.add(task2);
    }

    @After
    public void cleanTest() throws Exception {
        taskService.removeAll();
    }

    @Test
    public void findAllByUserIdTest() throws Exception {
        Assert.assertEquals(2, taskService.findAll(UserUtil.getUserId()).size());
    }

    @Test
    public void findOneByIdAndUserIdTest() throws Exception {
        Assert.assertNotNull(taskService.findOneById(UserUtil.getUserId(), task1.getId()));
        Assert.assertNotNull(taskService.findOneById(UserUtil.getUserId(), task2.getId()));
    }

    @Test(expected = ModelNotFoundException.class)
    public void removeByIdAndUserIdTest() throws Exception {
        @NotNull final List<TaskWebDto> tasks = new ArrayList<>();
        tasks.add(task1);
        Assert.assertNotNull(taskService.findOneById(UserUtil.getUserId(), task1.getId()));
        taskService.removeOneById(UserUtil.getUserId(), task1.getId());
        //Expected = ModelNotFoundException cause deleted
        taskService.findOneById(UserUtil.getUserId(), task1.getId());
    }

    @Test
    public void removeByUserIdTest() throws Exception {
        taskService.removeOne(UserUtil.getUserId(), task1);
        taskService.removeOne(UserUtil.getUserId(), task2);
        Assert.assertEquals(0, taskService.findAll(UserUtil.getUserId()).size());
    }

}
