package ru.t1.amsmirnov.taskmanager.unit.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.t1.amsmirnov.taskmanager.client.TaskClient;
import ru.t1.amsmirnov.taskmanager.config.DatabaseConfiguration;
import ru.t1.amsmirnov.taskmanager.config.WebApplicationConfiguration;
import ru.t1.amsmirnov.taskmanager.dto.TaskWebDto;
import ru.t1.amsmirnov.taskmanager.marker.DBCategory;
import ru.t1.amsmirnov.taskmanager.util.UserUtil;

import javax.ws.rs.core.MediaType;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebApplicationConfiguration.class, DatabaseConfiguration.class})
@Category(DBCategory.class)
public class TaskEndpointImplTest {

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    private MockMvc mockMvc;

    @NotNull
    @Autowired
    private WebApplicationContext wac;

    private static final String BASE_URL = "http://localhost:8080/api/tasks/";

    @NotNull
    private final TaskWebDto task1 = new TaskWebDto("Test Task 1");

    @NotNull
    private final TaskWebDto task2 = new TaskWebDto("Test Task 2");


    private final TaskClient client = TaskClient.client(BASE_URL);

    @Before
    public void initTest() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("test", "test");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        task1.setUserId(UserUtil.getUserId());
        task2.setUserId(UserUtil.getUserId());
        add(task1);
        add(task2);
    }

    @After
    public void clean() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete(BASE_URL + "deleteAll")
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    private TaskWebDto findById(@NotNull final String id) throws Exception {
        @NotNull String url = BASE_URL + "findById/" + id;
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        if (json.equals("")) return null;
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, TaskWebDto.class);
    }

    @Test
    public void findByIdTest() throws Exception {
        add(task1);
        final TaskWebDto task = findById(task1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getId(), task1.getId());
    }

    @Test
    public void deleteTest() throws Exception {
        @NotNull String url = BASE_URL + "delete";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(task2);
        mockMvc.perform(MockMvcRequestBuilders.delete(url).content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void deleteByIdTest() throws Exception {
        @NotNull String url = BASE_URL + "delete/" + task1.getId();
        mockMvc.perform(MockMvcRequestBuilders.delete(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void saveTest() throws Exception {
        add(task2);
        final TaskWebDto task = findById(task2.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(task2.getId(), task.getId());
    }

    private void add(@NotNull final TaskWebDto task) throws Exception {
        @NotNull String url = BASE_URL + "save";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(task);
        mockMvc.perform(MockMvcRequestBuilders.post(url).content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

}
