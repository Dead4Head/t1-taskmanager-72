package ru.t1.amsmirnov.taskmanager.integration.soap;

import org.apache.cxf.helpers.CastUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.http.HttpHeaders;
import ru.t1.amsmirnov.taskmanager.api.IAuthEndpoint;
import ru.t1.amsmirnov.taskmanager.api.ITaskEndpoint;
import ru.t1.amsmirnov.taskmanager.client.AuthSoapClient;
import ru.t1.amsmirnov.taskmanager.client.TaskSoapClient;
import ru.t1.amsmirnov.taskmanager.dto.TaskWebDto;
import ru.t1.amsmirnov.taskmanager.marker.IntegrationCategory;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.soap.SOAPFaultException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Category(IntegrationCategory.class)
public class TestSoapEndpointTest {

    @NotNull
    private static final String TEST_USER = "test";

    @NotNull
    private static final String TEST_PASS = "test";

    @NotNull
    private static final String URL = "http://localhost:8080";

    @NotNull
    private static IAuthEndpoint authEndpoint;

    @NotNull
    private static ITaskEndpoint taskEndpoint;

    @Nullable
    private static TaskWebDto task;

    @NotNull
    final String test_text = "TEST_SOAP_ENDPOINT";

    @BeforeClass
    public static void beforeClass() throws Exception {
        authEndpoint = AuthSoapClient.getInstance(URL);
        Assert.assertTrue(authEndpoint.login(TEST_USER, TEST_PASS).getSuccess());
        taskEndpoint = TaskSoapClient.getInstance(URL);
        @NotNull final BindingProvider authBindingProvider = (BindingProvider) authEndpoint;
        @NotNull final BindingProvider taskBindingProvider = (BindingProvider) taskEndpoint;
        Map<String, List<String>> headers = CastUtils.cast((Map) authBindingProvider.getResponseContext().get(MessageContext.HTTP_RESPONSE_HEADERS));
        if (headers == null) headers = new HashMap<>();
        @NotNull final Object cookieValue = headers.get(HttpHeaders.SET_COOKIE);
        @NotNull final List<String> cookies = (List<String>) cookieValue;
        headers.put(HttpHeaders.COOKIE, Collections.singletonList(cookies.get(0)));
        taskBindingProvider.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS, headers);
    }

    @Before
    public void before() {
        task = taskEndpoint.create(test_text, test_text);
    }

    @After
    public void after() {
        taskEndpoint.deleteAll();
    }

    @Test
    public void testCreate() {
        final String text = test_text + "_CREATE";
        task = taskEndpoint.create(text, text);
        final TaskWebDto foundTask = taskEndpoint.findById(task.getId());
        Assert.assertEquals(task.getId(), foundTask.getId());
    }

    @Test
    public void testFindById() {
        final TaskWebDto foundedTask = taskEndpoint.findById(task.getId());
        Assert.assertEquals(task.getName(), foundedTask.getName());
        Assert.assertEquals(task.getId(), foundedTask.getId());
    }

    @Test(expected = SOAPFaultException.class)
    public void testDelete() {
        taskEndpoint.delete(task.getId());
        taskEndpoint.findById(task.getId());
    }

}
