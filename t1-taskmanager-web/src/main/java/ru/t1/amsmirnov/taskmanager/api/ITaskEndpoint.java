package ru.t1.amsmirnov.taskmanager.api;

import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.t1.amsmirnov.taskmanager.dto.TaskWebDto;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@RequestMapping(value = "/api/tasks", produces = "application/json")
public interface ITaskEndpoint {

    @Nullable
    @WebMethod
    TaskWebDto create(
            @WebParam(name = "name", partName = "name") String name,
            @WebParam(name = "description", partName = "description") String desc
    );

    @Nullable
    @WebMethod
    List<TaskWebDto> findAll();

    @Nullable
    @WebMethod
    TaskWebDto findById(@WebParam(name = "id", partName = "id") String id);

    @Nullable
    @WebMethod
    TaskWebDto save(@WebParam(name = "task", partName = "task") TaskWebDto task);

    @Nullable
    @WebMethod
    TaskWebDto delete(@WebParam(name = "task", partName = "task") TaskWebDto task);

    @Nullable
    @WebMethod(operationName = "deleteById")
    TaskWebDto delete(@WebParam(name = "id", partName = "id") String id);

    @WebMethod
    void deleteAll();

}
