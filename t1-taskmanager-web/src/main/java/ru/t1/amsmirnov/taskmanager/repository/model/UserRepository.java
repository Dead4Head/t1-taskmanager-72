package ru.t1.amsmirnov.taskmanager.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.model.UserWeb;

public interface UserRepository extends AbstractRepository<UserWeb> {

    @Nullable
    UserWeb findByLogin(@NotNull String login);

    boolean existsByLogin(@NotNull String login);


}
