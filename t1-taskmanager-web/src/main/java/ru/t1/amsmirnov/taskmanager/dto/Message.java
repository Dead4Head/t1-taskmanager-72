package ru.t1.amsmirnov.taskmanager.dto;

import org.jetbrains.annotations.Nullable;

public class Message {

    @Nullable
    private String value;

    public Message() {
    }

    public Message(@Nullable String value) {
        this.value = value;
    }

    @Nullable
    public String getValue() {
        return value;
    }

    public void setValue(@Nullable final String value) {
        this.value = value;
    }

}
