package ru.t1.amsmirnov.taskmanager.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class Result {

    @NotNull
    private Boolean success = true;

    @Nullable
    private String message = "";

    public Result() {
    }

    public Result(@NotNull final Boolean success) {
        this.success = success;
    }

    public Result(@NotNull final Exception e) {
        success = false;
        message = e.getMessage();
    }

    @NotNull
    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(@NotNull final Boolean success) {
        this.success = success;
    }

    @Nullable
    public String getMessage() {
        return message;
    }

    public void setMessage(@Nullable final String message) {
        this.message = message;
    }

}
