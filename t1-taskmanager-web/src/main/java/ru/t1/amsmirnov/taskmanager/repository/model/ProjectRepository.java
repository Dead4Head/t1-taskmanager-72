package ru.t1.amsmirnov.taskmanager.repository.model;

import ru.t1.amsmirnov.taskmanager.model.ProjectWeb;

public interface ProjectRepository extends AbstractUserOwnedRepository<ProjectWeb> {

}
