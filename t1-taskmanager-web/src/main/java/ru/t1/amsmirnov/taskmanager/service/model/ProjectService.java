package ru.t1.amsmirnov.taskmanager.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.amsmirnov.taskmanager.enumerated.Status;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.field.IdEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.field.NameEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.field.UserIdEmptyException;
import ru.t1.amsmirnov.taskmanager.model.ProjectWeb;
import ru.t1.amsmirnov.taskmanager.repository.model.ProjectRepository;

@Service
public class ProjectService extends AbstractUserOwnedModelService<ProjectWeb, ProjectRepository> {

    @Autowired
    public ProjectService(@NotNull final ProjectRepository repository) {
        super(repository);
    }

    @NotNull
    @Transactional
    public ProjectWeb create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final ProjectWeb project = new ProjectWeb();
        project.setName(name);
        project.setDescription(description);
        return add(project);
    }

    @NotNull
    @Transactional
    public ProjectWeb updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final ProjectWeb project = findOneById(id);
        project.setName(name);
        project.setDescription(description);
        return update(project);
    }

    @NotNull
    @Transactional
    public ProjectWeb changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final ProjectWeb project = findOneById(id);
        if (status == null) return project;
        project.setStatus(status);
        return update(project);
    }

}