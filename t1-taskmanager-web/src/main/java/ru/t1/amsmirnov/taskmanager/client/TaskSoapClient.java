package ru.t1.amsmirnov.taskmanager.client;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.api.ITaskEndpoint;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;

public class TaskSoapClient {

    public static ITaskEndpoint getInstance(@NotNull final String baseUrl) throws MalformedURLException {
        @NotNull final String wsdl = baseUrl + "/ws/TaskEndpoint?wsdl";
        @NotNull final URL url = new URL(wsdl);
        @NotNull final String ns = "http://endpoint.taskmanager.amsmirnov.t1.ru/";
        @NotNull final String service = "TaskEndpointImplService";
        @NotNull final QName name = new QName(ns, service);
        @NotNull final ITaskEndpoint result = Service.create(url, name).getPort(ITaskEndpoint.class);
        @NotNull final BindingProvider bindingProvider = (BindingProvider) result;
        bindingProvider.getRequestContext().put(BindingProvider.SESSION_MAINTAIN_PROPERTY, true);
        return result;
    }

}
