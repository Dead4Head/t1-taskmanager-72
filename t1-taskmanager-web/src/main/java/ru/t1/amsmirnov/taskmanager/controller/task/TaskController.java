package ru.t1.amsmirnov.taskmanager.controller.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.amsmirnov.taskmanager.dto.CustomUser;
import ru.t1.amsmirnov.taskmanager.dto.TaskWebDto;
import ru.t1.amsmirnov.taskmanager.enumerated.Status;
import ru.t1.amsmirnov.taskmanager.service.dto.ProjectDtoService;
import ru.t1.amsmirnov.taskmanager.service.dto.TaskDtoService;

@Controller
public class TaskController {

    @Autowired
    private TaskDtoService taskService;

    @Autowired
    private ProjectDtoService projectService;

    @PostMapping("/task/create")
    public ModelAndView create(
            @AuthenticationPrincipal final CustomUser user
    ) {
        try {
            taskService.create(user.getUserId(), "Task_" + System.currentTimeMillis(), "");
            return new ModelAndView("redirect:/tasks");
        } catch (Exception e) {
            return errorView(e);
        }
    }

    @PostMapping("/task/delete/{id}")
    public ModelAndView delete(
            @AuthenticationPrincipal final CustomUser user,
            @PathVariable("id") String id
    ) {
        try {
            taskService.removeOneById(user.getUserId(), id);
            return new ModelAndView("redirect:/tasks");
        } catch (Exception e) {
            return errorView(e);
        }
    }

    @GetMapping("/task/edit/{id}")
    public ModelAndView edit(
            @AuthenticationPrincipal final CustomUser user,
            @PathVariable("id") String id
    ) {
        try {
            final TaskWebDto task = taskService.findOneById(user.getUserId(), id);
            if (task == null)
                return new ModelAndView("redirect:/tasks");
            ModelAndView modelAndView = new ModelAndView();
            modelAndView.setViewName("task-edit");
            modelAndView.addObject("task", task);
            modelAndView.addObject("statuses", Status.values());
            modelAndView.addObject("projects", projectService.findAll(user.getUserId()));
            return modelAndView;
        } catch (Exception e) {
            return errorView(e);
        }
    }

    @PostMapping("/task/edit/{id}")
    public ModelAndView edit(
            @AuthenticationPrincipal final CustomUser user,
            @ModelAttribute("task") TaskWebDto task,
            BindingResult result
    ) {
        try {
            if ("".equals(task.getProjectId())) task.setProjectId(null);
            taskService.update(user.getUserId(), task);
            return new ModelAndView("redirect:/tasks");
        } catch (Exception e) {
            return errorView(e);
        }
    }

    private ModelAndView errorView(final Exception e) {
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("error");
        modelAndView.addObject("error", e);
        return modelAndView;
    }

}
