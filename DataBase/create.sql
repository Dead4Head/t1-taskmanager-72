-- Table: public.tm_user

-- DROP TABLE IF EXISTS public.tm_user;

CREATE TABLE IF NOT EXISTS public.tm_user
(
    id character varying(255) COLLATE pg_catalog."default" NOT NULL,
    created timestamp without time zone NOT NULL,
    email character varying(255) COLLATE pg_catalog."default" NOT NULL,
    fst_name character varying(255) COLLATE pg_catalog."default",
    lst_name character varying(255) COLLATE pg_catalog."default",
    locked boolean NOT NULL,
    login character varying(255) COLLATE pg_catalog."default" NOT NULL,
    mid_name character varying(255) COLLATE pg_catalog."default",
    password character varying(255) COLLATE pg_catalog."default" NOT NULL,
    role character varying(255) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT tm_user_pkey PRIMARY KEY (id)
)


-- Table: public.tm_session

-- DROP TABLE IF EXISTS public.tm_session;

CREATE TABLE IF NOT EXISTS public.tm_session
(
    id character varying(255) COLLATE pg_catalog."default" NOT NULL,
    created timestamp without time zone NOT NULL,
    user_id character varying(255) COLLATE pg_catalog."default",
    role character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT tm_session_pkey PRIMARY KEY (id),
    CONSTRAINT tm_session_user_id_fk FOREIGN KEY (user_id)
        REFERENCES public.tm_user (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
