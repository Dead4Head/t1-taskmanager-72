package ru.t1.amsmirnov.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import ru.t1.amsmirnov.taskmanager.api.service.dto.IProjectDtoService;
import ru.t1.amsmirnov.taskmanager.api.service.dto.IProjectTaskDtoService;
import ru.t1.amsmirnov.taskmanager.api.service.dto.ITaskDtoService;
import ru.t1.amsmirnov.taskmanager.dto.model.ProjectDTO;
import ru.t1.amsmirnov.taskmanager.dto.model.TaskDTO;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.entity.ProjectNotFoundException;
import ru.t1.amsmirnov.taskmanager.exception.entity.TaskNotFoundException;
import ru.t1.amsmirnov.taskmanager.exception.field.ProjectIdEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.field.TaskIdEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.field.UserIdEmptyException;
import ru.t1.amsmirnov.taskmanager.marker.DBCategory;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@Category(DBCategory.class)
public class ProjectTaskServiceTest extends AbstractUserOwnedServiceTest {

    @NotNull
    private final IProjectDtoService projectDtoService = context.getBean(IProjectDtoService.class);

    @NotNull
    private final ITaskDtoService taskDtoService = context.getBean(ITaskDtoService.class);

    @NotNull
    @Autowired
    private final IProjectTaskDtoService projectTaskService = context.getBean(IProjectTaskDtoService.class);

    @NotNull
    private final List<TaskDTO> bindedTasks = new ArrayList<>();

    @NotNull
    private final List<TaskDTO> unbindedTasks = new ArrayList<>();

    @NotNull
    private ProjectDTO project;

    @Before
    public void initRepository() throws Exception {
        project = new ProjectDTO();
        project.setName("ProjectDTO");
        project.setDescription("Description");
        project.setUserId(USER_ALFA_ID);
        projectDtoService.add(project);

        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            Thread.sleep(2);
            @NotNull final TaskDTO task = new TaskDTO();
            task.setName("TaskDTO binded name: " + (NUMBER_OF_ENTRIES - i));
            task.setDescription("TaskDTO binded description: " + i);
            task.setUserId(USER_ALFA_ID);
            task.setProjectId(project.getId());
            taskDtoService.add(task);
            bindedTasks.add(task);
        }

        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            Thread.sleep(2);
            @NotNull final TaskDTO task = new TaskDTO();
            task.setName("TaskDTO unbinded name: " + i);
            task.setDescription("TaskDTO unbinded description: " + i);
            task.setUserId(USER_ALFA_ID);
            taskDtoService.add(task);
            unbindedTasks.add(task);
        }
    }

    @After
    public void clearRepositories() {
        taskDtoService.removeAll();
        projectDtoService.removeAll();
    }

    @Test
    public void testBindTaskToProject() throws AbstractException {
        for (final TaskDTO task : unbindedTasks) {
            assertNull(task.getProjectId());
            projectTaskService.bindTaskToProject(USER_ALFA_ID, project.getId(), task.getId());
            assertEquals(project.getId(), taskDtoService.findOneById(task.getId()).getProjectId());
        }
    }

    @Test(expected = UserIdEmptyException.class)
    public void testBindTaskToProject_UserIdEmptyException_1() throws AbstractException {
        projectTaskService.bindTaskToProject("", NONE_STR, NONE_STR);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testBindTaskToProject_UserIdEmptyException_2() throws AbstractException {
        projectTaskService.bindTaskToProject(NULL_STR, NONE_STR, NONE_STR);
    }

    @Test(expected = ProjectIdEmptyException.class)
    public void testBindTaskToProject_ProjectIdEmptyException_1() throws AbstractException {
        projectTaskService.bindTaskToProject(USER_ALFA_ID, "", NONE_STR);
    }

    @Test(expected = ProjectIdEmptyException.class)
    public void testBindTaskToProject_ProjectIdEmptyException_2() throws AbstractException {
        projectTaskService.bindTaskToProject(USER_ALFA_ID, NULL_STR, NONE_STR);
    }

    @Test(expected = TaskIdEmptyException.class)
    public void testBindTaskToProject_TaskIdEmptyException_1() throws AbstractException {
        projectTaskService.bindTaskToProject(USER_ALFA_ID, NONE_STR, "");
    }

    @Test(expected = TaskIdEmptyException.class)
    public void testBindTaskToProject_TaskIdEmptyException_2() throws AbstractException {
        projectTaskService.bindTaskToProject(USER_ALFA_ID, NONE_STR, NULL_STR);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testBindTaskToProject_ProjectNotFoundException() throws AbstractException {
        projectTaskService.bindTaskToProject(NONE_STR, project.getId(), NONE_STR);
    }

    @Test(expected = TaskNotFoundException.class)
    public void testBindTaskToProject_TaskNotFoundException() throws AbstractException {
        projectTaskService.bindTaskToProject(USER_ALFA_ID, project.getId(), NONE_STR);
    }

    @Test
    public void testRemoveProjectById() throws AbstractException {
        assertNotEquals(0, taskDtoService.findAllByProjectId(USER_ALFA_ID, project.getId()).size());
        assertTrue(projectDtoService.existById(project.getId()));
        projectTaskService.removeProjectById(USER_ALFA_ID, project.getId());
        assertFalse(projectDtoService.existById(project.getId()));
        assertEquals(0, taskDtoService.findAllByProjectId(USER_ALFA_ID, project.getId()).size());
    }

    @Test(expected = UserIdEmptyException.class)
    public void testRemoveProjectById_UserIdEmptyException_1() throws AbstractException {
        projectTaskService.removeProjectById("", NONE_STR);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testRemoveProjectById_UserIdEmptyException_2() throws AbstractException {
        projectTaskService.removeProjectById(NULL_STR, NONE_STR);
    }

    @Test(expected = ProjectIdEmptyException.class)
    public void testRemoveProjectById_ProjectIdEmptyException_1() throws AbstractException {
        projectTaskService.removeProjectById(USER_ALFA_ID, "");
    }

    @Test(expected = ProjectIdEmptyException.class)
    public void testRemoveProjectById_ProjectIdEmptyException_2() throws AbstractException {
        projectTaskService.removeProjectById(USER_ALFA_ID, NULL_STR);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testRemoveProjectById_ProjectNotFoundException() throws AbstractException {
        projectTaskService.removeProjectById(USER_ALFA_ID, NONE_STR);
    }

    @Test
    public void testUnbindTaskFromProject() throws AbstractException {
        for (final TaskDTO task : bindedTasks) {
            assertEquals(project.getId(), task.getProjectId());
            projectTaskService.unbindTaskFromProject(USER_ALFA_ID, project.getId(), task.getId());
            assertNull(taskDtoService.findOneById(task.getId()).getProjectId());
        }
    }

    @Test(expected = UserIdEmptyException.class)
    public void testUnbindTaskFromProject_UserIdEmptyException_1() throws AbstractException {
        projectTaskService.unbindTaskFromProject("", NONE_STR, NONE_STR);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testUnbindTaskFromProject_UserIdEmptyException_2() throws AbstractException {
        projectTaskService.unbindTaskFromProject(NULL_STR, NONE_STR, NONE_STR);
    }

    @Test(expected = ProjectIdEmptyException.class)
    public void testUnbindTaskFromProject_ProjectIdEmptyException_1() throws AbstractException {
        projectTaskService.unbindTaskFromProject(USER_ALFA_ID, "", NONE_STR);
    }

    @Test(expected = ProjectIdEmptyException.class)
    public void testUnbindTaskFromProject_ProjectIdEmptyException_2() throws AbstractException {
        projectTaskService.unbindTaskFromProject(USER_ALFA_ID, NULL_STR, NONE_STR);
    }

    @Test(expected = TaskIdEmptyException.class)
    public void testUnbindTaskFromProject_TaskIdEmptyException_1() throws AbstractException {
        projectTaskService.unbindTaskFromProject(USER_ALFA_ID, NONE_STR, "");
    }

    @Test(expected = TaskIdEmptyException.class)
    public void testUnbindTaskFromProject_TaskIdEmptyException_2() throws AbstractException {
        projectTaskService.unbindTaskFromProject(USER_ALFA_ID, NONE_STR, NULL_STR);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testUnbindTaskFromProject_ProjectNotFoundException() throws AbstractException {
        projectTaskService.unbindTaskFromProject(NONE_STR, project.getId(), NONE_STR);
    }

    @Test(expected = TaskNotFoundException.class)
    public void testUnbindTaskFromProject_TaskNotFoundException() throws AbstractException {
        projectTaskService.unbindTaskFromProject(USER_ALFA_ID, project.getId(), NONE_STR);
    }

}
