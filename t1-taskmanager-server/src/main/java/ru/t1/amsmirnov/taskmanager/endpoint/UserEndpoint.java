package ru.t1.amsmirnov.taskmanager.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.t1.amsmirnov.taskmanager.api.endpoint.IUserEndpoint;
import ru.t1.amsmirnov.taskmanager.api.service.ILoggerService;
import ru.t1.amsmirnov.taskmanager.api.service.IServiceLocator;
import ru.t1.amsmirnov.taskmanager.api.service.dto.IUserDtoService;
import ru.t1.amsmirnov.taskmanager.dto.model.SessionDTO;
import ru.t1.amsmirnov.taskmanager.dto.model.UserDTO;
import ru.t1.amsmirnov.taskmanager.dto.request.user.*;
import ru.t1.amsmirnov.taskmanager.dto.response.user.*;
import ru.t1.amsmirnov.taskmanager.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Controller
@WebService(endpointInterface = "ru.t1.amsmirnov.taskmanager.api.endpoint.IUserEndpoint")
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    @NotNull
    @Autowired
    private IUserDtoService userService;

    @NotNull
    @Autowired
    private ILoggerService loggerService;

    public UserEndpoint() {
        super();
    }

    @NotNull
    @Override
    @WebMethod
    public UserChangePasswordResponse changePassword(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserChangePasswordRequest request
    ) {
        try {
            @NotNull final SessionDTO session = check(request);
            @Nullable final String id = session.getUserId();
            @Nullable final String password = request.getPassword();
            @NotNull final UserDTO user = userService.setPassword(id, password);
            return new UserChangePasswordResponse(user);
        } catch (@NotNull final Exception e) {
            loggerService.error(e);
            return new UserChangePasswordResponse(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public UserLockResponse lockUserByLogin(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLockRequest request
    ) {
        try {
            check(request, Role.ADMIN);
            @Nullable final String login = request.getLogin();
            userService.lockUserByLogin(login);
            return new UserLockResponse();
        } catch (@NotNull final Exception e) {
            loggerService.error(e);
            return new UserLockResponse(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public UserRegistryResponse registry(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserRegistryRequest request
    ) {
        try {
            @Nullable final String login = request.getLogin();
            @Nullable final String password = request.getPassword();
            @Nullable final String email = request.getEmail();
            @NotNull final UserDTO user = userService.create(login, password, email);
            return new UserRegistryResponse(user);
        } catch (@NotNull final Exception e) {
            loggerService.error(e);
            return new UserRegistryResponse(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public UserRemoveResponse removeByLogin(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserRemoveRequest request
    ) {
        try {
            check(request, Role.ADMIN);
            @Nullable final String login = request.getLogin();
            @NotNull final UserDTO user = userService.removeByLogin(login);
            return new UserRemoveResponse(user);
        } catch (@NotNull final Exception e) {
            loggerService.error(e);
            return new UserRemoveResponse(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public UserRemoveByEmailResponse removeByEmail(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserRemoveByEmailRequest request
    ) {
        try {
            check(request, Role.ADMIN);
            @Nullable final String email = request.getEmail();
            @NotNull final UserDTO user = userService.removeByEmail(email);
            return new UserRemoveByEmailResponse(user);
        } catch (@NotNull final Exception e) {
            loggerService.error(e);
            return new UserRemoveByEmailResponse(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public UserUnlockResponse unlockUserByLogin(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserUnlockRequest request
    ) {
        try {
            check(request, Role.ADMIN);
            @Nullable final String login = request.getLogin();
            userService.unlockUserByLogin(login);
            return new UserUnlockResponse();
        } catch (@NotNull final Exception e) {
            loggerService.error(e);
            return new UserUnlockResponse(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public UserProfileResponse viewProfile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserProfileRequest request
    ) {
        try {
            @NotNull final SessionDTO session = check(request);
            @Nullable final String userId = session.getUserId();
            @NotNull final UserDTO user = userService.findOneById(userId);
            return new UserProfileResponse(user);
        } catch (@NotNull final Exception e) {
            loggerService.error(e);
            return new UserProfileResponse(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public UserUpdateResponse updateUserById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserUpdateRequest request
    ) {
        try {
            @NotNull final SessionDTO session = check(request);
            @Nullable final String id = session.getUserId();
            @Nullable final String firstName = request.getFirstName();
            @Nullable final String lastName = request.getLastName();
            @Nullable final String middleName = request.getMiddleName();
            @NotNull final UserDTO user = userService.updateUserById(id, firstName, lastName, middleName);
            return new UserUpdateResponse(user);
        } catch (@NotNull final Exception e) {
            loggerService.error(e);
            return new UserUpdateResponse(e);
        }
    }

}
