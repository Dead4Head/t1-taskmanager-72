package ru.t1.amsmirnov.taskmanager.util;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.exception.util.CryptException;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Base64;

public interface CryptUtil {

    @NotNull
    String TYPE = "AES/ECB/PKCS5Padding";

    @NotNull
    static SecretKeySpec getKey(@NotNull final String secretKey) throws CryptException {
        try {
            @NotNull final MessageDigest sha = MessageDigest.getInstance("SHA-1");
            @NotNull final byte[] key = secretKey.getBytes();
            @NotNull final byte[] digest = sha.digest(key);
            @NotNull final byte[] secret = Arrays.copyOf(digest, 16);
            return new SecretKeySpec(secret, "AES");
        } catch (final Exception e) {
            throw new CryptException(e);
        }
    }

    @NotNull
    static String encrypt(
            @NotNull final String secret,
            @NotNull final String strToEncrypt
    ) throws CryptException {
        try {
            @NotNull final SecretKeySpec secretKey = getKey(secret);
            @NotNull final Cipher cipher = Cipher.getInstance(TYPE);
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            @NotNull final byte[] bytes = strToEncrypt.getBytes(StandardCharsets.UTF_8);
            return Base64.getEncoder().encodeToString(cipher.doFinal(bytes));
        } catch (final Exception e) {
            throw new CryptException(e);
        }
    }

    @NotNull
    static String decrypt(
            @NotNull final String secret,
            @NotNull final String strToDecrypt
    ) throws CryptException {
        try {
            @NotNull final SecretKeySpec secretKey = getKey(secret);
            @NotNull final Cipher cipher = Cipher.getInstance(TYPE);
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            return new String(cipher.doFinal(Base64.getDecoder().decode(strToDecrypt)));
        } catch (final Exception e) {
            throw new CryptException(e);
        }
    }

}
