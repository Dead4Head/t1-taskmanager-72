package ru.t1.amsmirnov.taskmanager.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import ru.t1.amsmirnov.taskmanager.api.service.IAuthService;
import ru.t1.amsmirnov.taskmanager.api.service.IServiceLocator;
import ru.t1.amsmirnov.taskmanager.dto.model.SessionDTO;
import ru.t1.amsmirnov.taskmanager.dto.request.AbstractUserRequest;
import ru.t1.amsmirnov.taskmanager.enumerated.Role;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.user.AccessDeniedException;

public abstract class AbstractEndpoint {

    @NotNull
    @Autowired
    protected IAuthService authService;

    public AbstractEndpoint() {

    }

    @NotNull
    protected SessionDTO check(@Nullable final AbstractUserRequest request, @Nullable final Role role) throws AbstractException {
        if (request == null || role == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        @Nullable final SessionDTO session = authService.validateToken(token);
        boolean check = session.getRole() != null && session.getRole().equals(role);
        if (!check) throw new AccessDeniedException();
        return session;
    }

    protected SessionDTO check(@Nullable final AbstractUserRequest request) throws AbstractException {
        if (request == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        return authService.validateToken(token);
    }

}
