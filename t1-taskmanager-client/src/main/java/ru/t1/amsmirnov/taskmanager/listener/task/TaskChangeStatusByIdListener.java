package ru.t1.amsmirnov.taskmanager.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.amsmirnov.taskmanager.dto.request.task.TaskChangeStatusByIdRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.task.TaskChangeStatusByIdResponse;
import ru.t1.amsmirnov.taskmanager.enumerated.Status;
import ru.t1.amsmirnov.taskmanager.event.ConsoleEvent;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.CommandException;
import ru.t1.amsmirnov.taskmanager.util.TerminalUtil;

@Component
public final class TaskChangeStatusByIdListener extends AbstractTaskListener {

    @NotNull
    public static final String NAME = "task-change-status-by-id";

    @NotNull
    public static final String DESCRIPTION = "Change task status by ID.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@taskChangeStatusByIdListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[CHANGE TASK STATUS BY ID]");
        System.out.println("ENTER ID: ");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS: ");
        @NotNull final String statusValue = TerminalUtil.nextLine();
        @NotNull final Status status = Status.toStatus(statusValue);
        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(getToken(), id, status);
        @NotNull final TaskChangeStatusByIdResponse response = taskEndpoint.changeTaskStatusById(request);
        if (!response.isSuccess())
            throw new CommandException(response.getMessage());
    }

}
