package ru.t1.amsmirnov.taskmanager.listener.data;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.amsmirnov.taskmanager.dto.request.data.DataBinLoadRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.data.DataBinLoadResponse;
import ru.t1.amsmirnov.taskmanager.event.ConsoleEvent;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.CommandException;

@Component
public final class DataBinLoadListener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "data-load-bin";

    @NotNull
    public static final String DESCRIPTION = "Load data from binary dump file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@dataBinLoadListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[DATA BINARY LOAD]");
        @NotNull final DataBinLoadRequest request = new DataBinLoadRequest(getToken());
        @NotNull final DataBinLoadResponse response = domainEndpoint.loadDataBin(request);
        if (!response.isSuccess())
            throw new CommandException(response.getMessage());
    }

}
