package ru.t1.amsmirnov.taskmanager.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.amsmirnov.taskmanager.event.ConsoleEvent;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.listener.AbstractListener;

@Component
public final class CommandListListener extends AbstractSystemListener {

    @NotNull
    public static final String NAME = "commands";

    @NotNull
    public static final String DESCRIPTION = "Show available commands.";

    @NotNull
    public static final String ARGUMENT = "-cmd";

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@commandListListener .isSystemCommand(#consoleEvent.name)")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[COMMANDS]");
        for (@Nullable final AbstractListener listener : listeners) {
            if (listener == null) continue;
            final String name = listener.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

}
